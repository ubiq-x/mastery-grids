# Mastery Grids
A Web application (client only) implementing a social educational progress visualization which also grants access to educational resources.


## Abstract
Educational software has become pervasive in the classroom.  In fact, many pieces of such software have been found to positively affect learning.  However, despite being designed to aid students, it is often underused.  The two approaches that have been helpful in ameliorating that low usage problem are open learning model and social visualization.  This repository introduces a fusion of these two ideas in a form of social progress visualization.  A classroom evaluation indicates that this combination may be effective in engaging students, guiding them to suitable content, and enabling faster content access.


## Contents
- [Features](#features)
- [Details](#details)
  * [A Grid](#a-grid)
  * [All Resources Mode](#all-resources-mode)
  * [Resource Focus Mode](#resource-focus-mode)
  * [Comparison View vs Individual View](#comparison-view-vs-individual-view)
  * [Topic Difficulty and Importance](#topic-difficulty-and-importance)
  * [Activities](#activities)
- [Project Status](#project-status)
- [Dependencies](#dependencies)
- [Data Structures](#data-structures)
- [Demo](#demo)
- [Live System](#live-system)
- [Customizing](#customizing)
- [Evaluation](#evaluation)
  * [Visualization Usage over Time](#visualization-usage-over-time)
- [Acknowledgments](#acknowledgments)
- [References](#references)
- [Citing](#citing)
- [License](#license)


## Features
- Visualization of:
  * Educational progress a learner is making
  * Educational progress the other learners (which can be organized into multiple groups) are making
  * Comparison of the learner's progress and the progress of any of the groups
  * Topic difficulty, importance towards the course completion, or similar variables
  * Best focus of one's learning efforts
- Generality in terms of educational space covered:
  * Can be used in any course
  * Can be used with possibly any type of educational activities (e.g., questions, examples, readings, lecture notes, or lecture videos)
- Fast and esthetically pleasing user interface


## Details
### A Grid
Mastery Grids uses a grid as its central visualization component (Figure 1).  Every grid is three-dimensional with the two geometric dimensions being _topics_ that learners need to master (e.g., "Variables" or "Arrays" in programming; the horizontal dimension) and educational aids or _resources_ they can access (e.g., questions, examples, readings, lecture notes or videos; the vertical dimension).  The third dimension is given by the intensity of the color in each cell of a grid and denotes the level of completeness, level of mastery, or level of progress a learner has achieved or demonstrated for the given combination of topic and resource (what the intensity means exactly depends on how the learner's progress is being modeled).  This design allows learners to quickly assess how they are doing in a course and where to best allocate their efforts.  As an important side note, this design also underlines the importance of correct learner modeling which is done behind the scenes; unless that modeling correctly reflects the current understanding of the learner, it may be misleading or even harmful.  For example, reassured by the visualization, a student may have an overly optimistic idea about their progress which may in turn lead them to shorten study time for certain topics possibly harming their eventual exam performance.

<p align="center" style="text-align: center;"><img src="media/grid.png" width="50%" alt="A grid" /></p>
<p align="center"><b>Figure 1.</b> A grid, the central visualization component.</p>

As shown on Figure 1, the user interface is color-coded with colors laying on opposite ends of the color wheel which makes it easier to directly compare individual grid cells.  In order to stand out, the cell with the mouse cursor over it experiences animated rotation by 45 degrees.

### All Resources Mode
Figure 2 shows the first of the two modes Mastery Grids can be in, the _All Resources_ mode.  In that mode, the top grid (i.e., _Me_) shows progress the learner has made, the bottom grid (i.e., _Group_) shows progress that the currently selected group has made, and the middle grid (i.e., _Me vs group_) shows the difference between the two grids.  By manipulating the group (using the toolbar on the top of the window) the learner chooses what they want to compare themselves to.  For example, they could choose "Class average" or "Top three learners."  What groups are available and how they are named is decided on by the server.  Further down, below the three main grids, are individual learner grids; each learner in the currently selected group is represented by one of those grids.

![All Resources mode](media/mastery-grids-01.png)
<p align="center"><b>Figure 2.</b> All Resources mode (i.e., both Questions and Examples resources shown).</p>

Judging small differences in color intensities of grid cells may be hard, especially for cells which are far apart.  To make that easier, a vertical bar chart is displayed to the right of a grid when the mouse cursor is over that grid (Figure 2).  That box plot provides a more quantitative way of judging comparative progress.  This is especially useful for the _Me-vs-group_ grid where the bars going up indicate the current learner has progressed more than the group and the bars going down indicate the opposite (Figure 3).  Staying on the subject of color intensities for a moment longer, because perception (including color perception) obeys the logarithmic law, raw values of progress from the range of _[0..1]_ are mapped to color intensities through a logarithmic function.  The [Wikipedia](https://en.wikipedia.org/wiki/Weber%E2%80%93Fechner_law) has a good treatment of this aspect of cognition.

### Resource Focus Mode
In the All Resources mode described above a learner sees the entirety of the material which is available to them (i.e., all topics and all resources).  A learner can, however, choose to focus on one particular resource (e.g., Examples) and thus enter the _Resource Focus_ mode (Figure 3).  In that mode, the three main grids are fused into a single main grid.  That single grid displays only the selected resource rows from the three main grids, i.e., the top row shows the learner, the bottom row shows the group, and the middle row shows the difference between the two.  The Resource Focus mode makes the me-vs-group comparison easier by displaying all relevant information in close proximity.  As a result of focusing on one resource type, all other learners from the current group are shown in a single grid as well.  The height of rows of that grid can be controlled to accommodate large groups of learners; short rows will allow groups of even several hundred learners to fit on one screen granting a good overview of the overall class progress.

![Resource Focus mode](media/mastery-grids-02.png)
<p align="center"><b>Figure 3.</b> Resource Focus mode (i.e., only the Questions resource shown).</p>

The Resource Focus mode features a timeline (Figure 3) which makes it easier for learners to identify which topic is currently being covered in class (the big circle).  Furthermore, green and red circles on the timeline respectively denote already-covered and to-be-covered topics.  This color-coding is significant in that green, in an unconsciously satisfying way, indicates topics learners should feel free to engage with.

### Comparison View vs Individual View
At any point, the learner can choose to focus mostly on their own progress by switching from the _Comparison view_ to the _Individual view_ which is shown on Figure 4.  In that view, it is still possible to see the progress that the entire class has made, but no obvious me-vs-others comparisons are present.  To clearly indicate that the Individual view has been engaged, the pink-and-blue color coding is substituted with grayscale all over the interface.  This view may be sought after by learners who are motivated by their own progress only and do not identify with herding behavior (see, e.g., Raafat et al., 2009).

![Resource Focus mode](media/mastery-grids-03.png)
<p align="center"><b>Figure 4.</b> Resource Focus mode with Individual view (i.e., lack of ostentatious comparisons with others).</p>

### Topic Difficulty and Importance
Learners may benefit from knowing how difficult each of the topics is.  It could also be useful for them to know how important each topic is towards the course goal as a whole.  As shown on Figure 5, Mastery Grids can display that sort of information by relating the width of a grid's cells to variables like difficulty or importance.  Those variables are defined by the course instructor and provided to the visualization from the server.

![Resource Focus mode](media/mastery-grids-04.png)
<p align="center"><b>Figure 5.</b> The size of topics can be displayed as commensurate to their difficulty or importance to the course goal.</p>

### Activities
Every cell from any of the main grids (e.g., the _Me_ grid) grants access to a list of activities.  When a cell is clicked, topic grid (or grids) are replaced by an activity grid (or grids), as shown in Figure 6.  Each cell of an activity grid corresponds to a single activity the learner can access and clicking on that cell loads that activity.

![Activities](media/mastery-grids-05.png)
<p align="center"><b>Figure 6.</b> List of activities for the topic "Arrays" (Resource Focus mode). Clicking on a grid cell accesses the underlying activity (see Figure 7).</p>

![An activity](media/mastery-grids-06.png)
<p align="center"><b>Figure 7.</b> An activity being displayed after a cell in a grid has been clicked.</p>

Mastery Grids does not impose anything on the activities that can be accessed through grid interaction.  Figure 7 shows a sample activity, an Example, displayed as an overlay in an HTML frame.  As I have mentioned before, the content of that frame could easily consist of assigned readings, lecture slides, or even a video recording of the relevant lecture.  In general, launching activities is only possible when Mastery Grids is integrated into an educational system where it acts as a user interface, but the demo described later in this document loads activities as well.


## Project Status
I use this repository to publish source code associated with the original article, Loboda et al. (2014).  While further work has been carried out as an extension to what I publish here, I see this code as a milestone that marks the end of my involvement in the project and hope it will be useful to someone in its own right.


## Dependencies
- [jQuery](https://jquery.com) 1.10+ (included)
- [d3](https://d3js.org) (included)
- Student model management server-side instance (not needed for the demo)


## Data Structures
Being only a user interface, the present software does not define any persistent data structures.  It does, however, define a data structure it expects from the server-side ([`data-anon.js`](data-anon.js)).


## Demo
Below is a link to the demo of this Web app.  Feel free to attempt to interact with all the grids (but not all of them are actually interactive) and don't forget to play around with the toolbar controls.  While this demo contains two resources, Questions and Exercises, only Examples work.  Even so, I cannot guarantee that this will not change because the Examples are loaded from an external educational service which I do not control and which may become inaccessible in the future.  Note also that while the demo uses data much like what a live server would send, it is in fact static data (stored in [`data-anon.js`](data-anon.js)).

Demo: [`http://ubiq-x.gitlab.io/mastery-grids/index.html?data-live=0`](http://ubiq-x.gitlab.io/mastery-grids/index.html?data-live=0)

To run the demo on your machine, simply clone this repository into your Web server and navigate your browser to:
```
http://<hostname>:<port>/<path>/mastery-grids/index.html?data-live=0
```

## Live System
The Web interface published here requires a server-side instance to manage models of all students spread across possibly multiple courses.  Implementing that instance is beyond this repository.  However, the protocol defined in [`script/protocol.js`](script/protocol.js) and static data from [`data-anon.js`](data-anon.js) should shed light on what this Web client is expecting to receive from the server.  That information should be helpful in reverse-engineering the logic layer.

Further inspiration for the back-end can be drawn from the [Course Authoring](https://gitlab.com/ubiq-x/course-authoring) project.  The interface presented there has been implemented to support creating content for Mastery Grids.


## Customizing
Mastery Grids can be called with query string arguments that control the visibility of certain elements of the user interface.  The [`launcher.html`](launcher.html) page sets those arguments (the two last sections on the page, i.e., _UI: Toolbar_ and _UI: Grids_).  You may also want to inspect [`script/vis.js`](script/vis.js).


## Evaluation
This user interface has been evaluated in three courses in the Fall 2013 at the School of Information Sciences, University of Pittsburgh.  The three courses were: Introduction to Object Oriented Programming (undergraduate) and Database Management (undergraduate and graduate).  Loboda et al. (2014) contains details on the evaluation.

The [`analysis`](analysis) directory contains the R scripts I used to obtain the results as well all output plots (a fraction of which was actually published).  I cannot provide data due to privacy concerns.

### Visualization Usage over Time
As one example of an analysis there was no room for in Loboda et al. (2014), and which therefore is also unfinished, below I look at survival curves that show the probability that an average student had a session with Mastery Grids (i.e., they logged in to access the live system) as a function of how far into the semester they were.  More formally, the curves are a result of fitting the proportional hazard intercept model _h(t) = h<sub>0</sub>(t)_ to different datasets.

![Survival curve: All courses](analysis/out/comb/10%20sess.prob.png)
<p align="center"><b>Figure 8.</b> The survival curve for all three courses combined.</p>

Figure 8 shows the survival curve when the data for all three courses are pooled.  The plot indicates that at about 50 days into the semester about 75% of students were still logging in to use Mastery Grids and the content it granted access to.  The proportion of students who kept using the visualization dropped to 50% at about 80-85 days and to 25% close to the semester's conclusion (i.e., after over three months).  It would be interesting to investigate the plausible causes of this usage decrease.  For example, is it that students got too busy with projects from these and other courses and could no longer devote the time to using the visualization?  Or perhaps they found the visualization increasingly less useful as they become more competent.

![Survival curve: Undergraduate Database](analysis/out/pitt.db.u/10%20sess.prob.png)
<p align="center"><b>Figure 9.</b> The survival curve for the undergraduate Database Management course.</p>

Looking at the survival curves per course is equally illuminating.  For example, Figure 9 shows the survival curve for the undergraduate level Database Management course, the only undergraduate course.  Of all three courses, this curve exhibits the slowest decay rate.  One possible explanation is that younger students found the visualization and/or the activities more useful than did their more senior counterparts.  Another possibility is that the undergraduates required more guidance in terms of what to do next.  Yet another one is that they were more susceptible to comparing themselves against their peers.  All those are interesting empirical questions which, by the way, assume the differences between this and the other survival curves are statistically significant.


## Acknowledgments
- I worked on this project as a member of the [PAWS lab](http://www.pitt.edu/~paws) at the University of Pittsburgh.
- I would like to thank the creators of [Color Brewer](http://colorbrewer2.org); it has been helpful in generating the color pallet for this visualization.  The pallet is stored in [`script/colorbrewer.js`](script/colorbrewer.js).
- The R code published as part of the [Creating good looking survival curves - the 'ggsurv' function](https://www.r-statistics.com/2013/07/creating-good-looking-survival-curves-the-ggsurv-function) blog post was helpful in obtaining some of the results.


## References
Loboda T.D., Guerra, J., Hosseini, R., & Brusilovsky P. (2014).  Mastery Grids: An Open Source Social Educational Progress Visualization.  _European Conference on Technology Enhanced Learning (EC-TEL)_, 235-248.  Software available at _https://gitlab.com/ubiq-x/mastery-grids_ [[PDF](loboda-2014-ectel.pdf)]

Loboda T.D., Guerra, J., Hosseini, R., & Brusilovsky P. (2014).  Mastery Grids: An Open-Source Social Educational Progress Visualization.  Poster presented at the _Innovation and Technology in Computer Science Education (ITiCSE)_, in Uppsala, Sweden.  [[PDF](loboda-2014-iticse.pdf)]

Raafat, R.M., Chater, N., & Frith, C. (2009) Herding in humans.  _Trends in Cognitive Sciences_.  13 (10): 420–428.


## Citing
If you would like to cite the work presented here, use the first references above (i.e., the EC-TEL article).  While my involvement has ended, more work has been done on this project.  If you are interested in those further progressions, it would behoove you to look for more up-to-date references.  Nevertheless, this is the original work.


## License
This project is licensed under the [BSD License](LICENSE.md).
